<?php

use App\Http\Controllers\FatSecretController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('/searchFood/{format}', [FatSecretController::class, 'searchFood']);
Route::get('/foodById/{format}', [FatSecretController::class, 'foodById']);
Route::get('/recipeTypes/{format}', [FatSecretController::class, 'recipeTypes']);
Route::get('/searchRecipe/{format}', [FatSecretController::class, 'searchRecipe']);
Route::get('/recipeById/{format}', [FatSecretController::class, 'recipeById']);
