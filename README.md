# Setup and run

```
docker-compose up
```

access `http://localhost:8000/`

swagger `http://localhost:8000/api/documentation`
