<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Http\Controllers\FatSecretAPI\FatSecretAPI;

class FatSecretController extends Controller
{
    /**
     * @OA\Get(
     *  path="/api/searchFood/{format}",
     *  operationId="fatSecretSearchFood",
     *  summary="Search Food FatSecret",
     *  @OA\Parameter(name="format",
     *    in="path",
     *    required=true,
     *    description="Return format, XML|JSON, default fallback XML",
     *    @OA\Schema(type="string")
     *  ),
     *  @OA\Parameter(name="search",
     *    in="query",
     *    required=true,
     *    description="Search string",
     *    @OA\Schema(type="string")
     *  ),
     *  @OA\Parameter(name="page",
     *    in="query",
     *    required=false,
     *    description="Page starting from 0, default 0",
     *    @OA\Schema(type="integer")
     *  ),
     *  @OA\Parameter(name="per_page",
     *    in="query",
     *    required=false,
     *    description="Items per page, default 20",
     *    @OA\Schema(type="integer")
     *  ),
     *  @OA\Response(response="200",
     *    description="Food data",
     *  )
     * )
     *
     * @param Request $request
     * @param string $format
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function searchFood(Request $request, string $format): Response
    {
        try {
            $fatSecret = new FatSecretAPI();

            if (empty($request->get('search'))) {
                throw new \Exception('Missing data');
            }

            $parameters = [
                'format' => $format,
                'search_expression' => $request->get('search'),
                'page_number' => $request->get('page') ?? 0,
                'max_results' => $request->get('per_page') && $request->get('per_page') > 0 && $request->get('per_page') <= 50
                    ? $request->get('per_page')
                    : 20,
            ];

            $data = $fatSecret->searchFood($parameters);

            if (strtolower($format) === 'json') {
                return response(json_decode($data, true));
            } else {
                return response($data);
            }
        } catch (\Exception $e) {
            if ($e->getCode() === 400) {
                return response($e->getMessage(), 400);
            }
            return response('Server error, please contact administrator!', 500);
        }
    }

    /**
     * @OA\Get(
     *  path="/api/foodById/{format}",
     *  operationId="fatSecretFoodById",
     *  summary="Food by ID FatSecret",
     *  @OA\Parameter(name="format",
     *    in="path",
     *    required=true,
     *    description="Return format, XML|JSON, default fallback XML",
     *    @OA\Schema(type="string")
     *  ),
     *  @OA\Parameter(name="food_id",
     *    in="query",
     *    required=true,
     *    description="Food ID string",
     *    @OA\Schema(type="string")
     *  ),
     *  @OA\Response(response="200",
     *    description="Food data",
     *  )
     * )
     *
     * @param Request $request
     * @param string $format
     * @return Response
     * @throws \Exception|\GuzzleHttp\Exception\GuzzleException
     */
    public function foodById(Request $request, string $format): Response
    {
        try {
            $fatSecret = new FatSecretAPI();

            if (empty($request->get('food_id'))) {
                throw new \Exception('Missing data');
            }

            $parameters = [
                'format' => $format,
                'food_id' => $request->get('food_id')
            ];

            $data = $fatSecret->foodById($parameters);

            if (strtolower($format) === 'json') {
                return response(json_decode($data, true));
            } else {
                return response($data);
            }
        } catch (\Exception $e) {
            if ($e->getCode() === 400) {
                return response($e->getMessage(), 400);
            }
            return response('Server error, please contact administrator!', 500);
        }
    }

    /**
     * @OA\Get(
     *  path="/api/recipeTypes/{format}",
     *  operationId="fatSecretRecipeTypes",
     *  summary="Food by ID FatSecret",
     *  @OA\Parameter(name="format",
     *    in="path",
     *    required=true,
     *    description="Return format, XML|JSON, default fallback XML",
     *    @OA\Schema(type="string")
     *  ),
     *  @OA\Response(response="200",
     *    description="Recipe Types data",
     *  )
     * )
     *
     * @param string $format
     * @return Response
     * @throws \Exception
     */
    public function recipeTypes(string $format): Response
    {
        try {
            if (empty($format)) {
                throw new \Exception('Missing data');
            }

            $fatSecret = new FatSecretAPI();

            $parameters = [
                'format' => $format
            ];

            $data = $fatSecret->recipeTypes($parameters);

            if (strtolower($format) === 'json') {
                return response(json_decode($data, true));
            } else {
                return response($data);
            }
        } catch (\Exception $e) {
            if ($e->getCode() === 400) {
                return response($e->getMessage(), 400);
            }
            return response('Server error, please contact administrator!', 500);
        }
    }


    /**
     * @OA\Get(
     *  path="/api/searchRecipe/{format}",
     *  operationId="fatSecretSearchRecipe",
     *  summary="Search Recipe FatSecret",
     *  @OA\Parameter(name="format",
     *    in="path",
     *    required=true,
     *    description="Return format, XML|JSON, default fallback XML",
     *    @OA\Schema(type="string")
     *  ),
     *  @OA\Parameter(name="search",
     *    in="query",
     *    required=true,
     *    description="Search string",
     *    @OA\Schema(type="string")
     *  ),
     *  @OA\Parameter(name="page",
     *    in="query",
     *    required=false,
     *    description="Page starting from 0, default 0",
     *    @OA\Schema(type="integer")
     *  ),
     *  @OA\Parameter(name="per_page",
     *    in="query",
     *    required=false,
     *    description="Items per page, default 20",
     *    @OA\Schema(type="integer")
     *  ),
     *  @OA\Response(response="200",
     *    description="Table data",
     *  )
     * )
     *
     * @param Request $request
     * @param string $format
     * @return Response
     * @throws \Exception
     */
    public function searchRecipe(Request $request, string $format): Response
    {
        try {
            if (empty($request->get('search'))) {
                throw new \Exception('Missing data', 400);
            }

            $parameters = [
                'format' => $format,
                'search_expression' => $request->get('search'),
                'page_number' => $request->get('page') ?? 0,
                'max_results' => $request->get('per_page') && $request->get('per_page') > 0 && $request->get('per_page') <= 50
                    ? $request->get('per_page')
                    : 20,
            ];

            $fatSecret = new FatSecretAPI();

            $data = $fatSecret->searchRecipes($parameters);

            if (strtolower($format) === 'json') {
                return response(json_decode($data, true));
            } else {
                return response($data);
            }
        } catch (\Exception $e) {
            if ($e->getCode() === 400) {
                return response($e->getMessage(), 400);
            }
            return response('Server error, please contact administrator!', 500);
        }
    }

    /**
     * @OA\Get(
     *  path="/api/recipeById/{format}",
     *  operationId="fatSecretRecipeById",
     *  summary="Food by ID FatSecret",
     *  @OA\Parameter(name="format",
     *    in="path",
     *    required=true,
     *    description="Return format, XML|JSON|PDF default fallback XML",
     *    @OA\Schema(type="string")
     *  ),
     *  @OA\Parameter(name="recipe_id",
     *    in="query",
     *    required=true,
     *    description="Recipe ID string",
     *    @OA\Schema(type="string")
     *  ),
     *  @OA\Response(response="200",
     *    description="Recipe data",
     *  )
     * )
     *
     * @param Request $request
     * @param string $format
     * @return Response
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function recipeById(Request $request, string $format): Response
    {
        try {
            if (empty($request->get('recipe_id'))) {
                throw new \Exception('Missing data', 400);
            }

            $parameters = [
                'format' => strtolower($format) === 'pdf' ? 'json' : strtolower($format),
                'recipe_id' => $request->get('recipe_id')
            ];

            $fatSecret = new FatSecretAPI();

            $data = $fatSecret->recipeById($parameters);

            if (strtolower($format) === 'pdf') {
                $data = $this->prepareDataForPDF(json_decode($data, true));
                $pdf = PDF::loadView('pdf.recipe', $data);
                return $pdf->download($data['recipe_name'] ?? 'recipe' . '.pdf');
            } elseif (strtolower($format) === 'json') {
                return response(json_decode($data, true));
            } else {
                return response($data);
            }
        } catch (\Exception $e) {
            if ($e->getCode() === 400) {
                return response($e->getMessage(), 400);
            }
            return response('Server error, please contact administrator!', 500);
        }
    }

    /**
     * Prepare data for PDF
     *
     * @param array $data
     * @return array
     */
    private function prepareDataForPDF(array $data): array
    {
        $returnData = [
            'directions' => $data['recipe']['directions']['direction'] ?? [],
            'ingredients' => $data['recipe']['ingredients']['ingredient'] ?? [],
            'preparation_time_min' => $data['recipe']['preparation_time_min'] ?? null,
            'cooking_time_min' => $data['recipe']['cooking_time_min'] ?? null,
            'number_of_servings' => $data['recipe']['number_of_servings'] ?? null,
            'recipe_name' => $data['recipe']['recipe_name'] ?? null,
            'recipe_image' => public_path('images/no_image.png'),
        ];

        if (isset($data['recipe']['recipe_images'])) {
            $returnData['recipe_image'] = $data['recipe']['recipe_images']['recipe_image'];
        }

        return $returnData;
    }
}
