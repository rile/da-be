<?php

namespace App\Http\Controllers\FatSecretAPI;

class FatSecretAPI
{
    /**
     * Prepare and send request to FatSecret
     *
     * @param string $method
     * @param string $url
     * @param array $options
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    private function fatSecretRequest(string $method, string $url, array $options): string
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request($method, $url, $options);

        $content = $response->getBody()->getContents();

        if (str_starts_with($content, '{"error":')) {
            throw new \Exception('Guzzle error', 500);
        }

        return $content;
    }

    /**
     * Get the access token
     *
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAccessToken(): string
    {
        $options = [
            "auth" => [
                env("FAT_SECRET_CLIENT_ID"),
                env("FAT_SECRET_CLIENT_SECRET")
            ],
            "form_params" => [
                'grant_type' => 'client_credentials',
                'scope' => 'basic'
            ]
        ];

        $responseData = $this->fatSecretRequest("POST", "https://oauth.fatsecret.com/connect/token", $options);

        return json_decode($responseData, true)['access_token'];
    }

    /**
     * Get list of searched food items.
     *
     * @param array $params
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function searchFood(array $params): string
    {
        $options = [
            "headers" => [
                "Content-Type" => "application/json",
                "Authorization" => "Bearer " . $this->getAccessToken()
            ]
        ];
        $urlParameters = [
            "method" => "foods.search"
        ];
        $urlParameters = array_merge($urlParameters, $params);

        $responseData = $this->fatSecretRequest("POST", "https://platform.fatsecret.com/rest/server.api?".http_build_query($urlParameters), $options);

        return $responseData;
    }

    /**
     * Get single food data.
     *
     * @param array $params
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function foodById(array $params): string
    {
        $options = [
            "headers" => [
                "Content-Type" => "application/json",
                "Authorization" => "Bearer " . $this->getAccessToken()
            ]
        ];
        $urlParameters = [
            "method" => "food.get.v2"
        ];

        $urlParameters = array_merge($urlParameters, $params);

        $responseData = $this->fatSecretRequest("POST", "https://platform.fatsecret.com/rest/server.api?".http_build_query($urlParameters), $options);

        return $responseData;
    }

    /**
     * Get recipe types list.
     *
     * @param array $params
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function recipeTypes(array $params): string
    {
        $options = [
            "headers" => [
                "Content-Type" => "application/json",
                "Authorization" => "Bearer " . $this->getAccessToken()
            ]
        ];

        $urlParameters = [
            "method" => "recipe_types.get"
        ];

        $urlParameters = array_merge($urlParameters, $params);

        $responseData = $this->fatSecretRequest("POST", "https://platform.fatsecret.com/rest/server.api?".http_build_query($urlParameters), $options);

        return $responseData;
    }

    /**
     * Get list of searched recipe items.
     *
     * @param array $params
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function searchRecipes(array $params): string
    {
        $options = [
            "headers" => [
                "Content-Type" => "application/json",
                "Authorization" => "Bearer " . $this->getAccessToken()
            ]
        ];
        $urlParameters = [
            "method" => "recipes.search"
        ];
        $urlParameters = array_merge($urlParameters, $params);

        $responseData = $this->fatSecretRequest("POST", "https://platform.fatsecret.com/rest/server.api?".http_build_query($urlParameters), $options);

        return $responseData;
    }

    /**
     * Get single recipe data.
     *
     * @param array $params
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function recipeById(array $params): string
    {
        $options = [
            "headers" => [
                "Content-Type" => "application/json",
                "Authorization" => "Bearer " . $this->getAccessToken()
            ]
        ];
        $urlParameters = [
            "method" => "recipe.get"
        ];

        $urlParameters = array_merge($urlParameters, $params);

        $responseData = $this->fatSecretRequest("POST", "https://platform.fatsecret.com/rest/server.api?".http_build_query($urlParameters), $options);

        return $responseData;
    }
}
