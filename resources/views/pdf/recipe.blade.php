<div>
    <div style="border-bottom: solid 2px black; margin: 1em; min-height: 250px;">
        <div style="width: 50%; float: left">
            <h1>{{ $recipe_name }}</h1>
            @if ($preparation_time_min)
                <h3>Preparation Time: <span style="color: red;">{{ $preparation_time_min }} min</span></h3>
            @endif
            @if ($cooking_time_min)
                <h3>Cooking Time: <span style="color: red;">{{ $cooking_time_min }} min</span></h3>
            @endif
            @if ($number_of_servings)
                <h3>Servings Time: <span style="color: red;">{{ $number_of_servings }}</span></h3>
            @endif
        </div>
        <div style="width: 50%; float: left;text-align: right;">
            <img src="{{ $recipe_image }}" alt="no image" height="175" style="margin-top: 1em;"/>
        </div>
        <div style="clear: both;"></div>
    </div>
    <div style="margin: 1em; min-height: 250px;">
        <div style="width: 49%; float: left; border-right: solid 2px black">
            <h3>Ingredients</h3>
            @foreach ($ingredients as $ingredient)
                <p>{{ $ingredient['food_name'] }} - {{ $ingredient['ingredient_description'] }}</p>
            @endforeach
        </div>
        <div style="width: 49%; float: left; padding-left: 1em;">
            <h3>Directions</h3>
            @foreach ($directions as $direction)
                <p>{{ $direction['direction_number'] }}. {{ $direction['direction_description'] }}</p>
            @endforeach
        </div>
    </div>
</div>
